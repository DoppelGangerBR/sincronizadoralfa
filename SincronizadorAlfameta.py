import time
import pyodbc as Conexao
import datetime as Data
from datetime import datetime
from datetime import timedelta
import os
import atexit
import sys
import LeArquivoIni as ConfiguracoesIni
from GeraLogs import Escrevelog
import MandaEmails

limpatela = lambda: os.system('cls') #Limpa a tela sempre que tenta sincronizar
CorConsole = lambda: os.system('color 1E'); #Define uma cor padrão para o console
CorConsoleComErro = lambda: os.system('color 47') #Define uma cor vermelha para quando acontece erros
DescricaoErro = ""
QuantidadeErros = 0
UltimaTentativa = ""
UltimaSincronizaoComSucesso = ""

def SincTotal():
    dataAgora = Data.datetime.now() #Pega data do dia e hora    
    ConfiguracoesIni.LeConfiguracoes() #Chama o metodo que le o arquivo .INI e carrega os valores nas variaveis    
    segundosParaProximaSincronizacaoEmCasoDeFalha = int(ConfiguracoesIni.IntervaloDeTempoEntreTentativasEmCasoDeErro)
    global UltimaSincronizaoComSucesso
    global UltimaTentativa
    global conexao
    global cursor
    global QuantidadeErros
    global DescricaoErro
    CorConsole() #Define a cor do console
    if QuantidadeErros == int(ConfiguracoesIni.QuantidadeTentativasEmCasoDeErro): #Se acontecerem mais que 3 erros, a aplicação para aqui e exibe informações sobre o oque aconteceu
        limpatela()        
        CorConsoleComErro() 
        print("Falha na sincronização!\nPor favor, contate o suporte\n\nDescrição do ultimo erro:\n"+DescricaoErro+"\n\nUltima operação com sucesso.: "+UltimaSincronizaoComSucesso+"\nUltima tentativa antes de parar.: "+UltimaTentativa)
        Escrevelog(2,DescricaoErro)
        MandaEmails.EnviaEmail(DescricaoErro)
        time.sleep(10)        
    else:
        try:
            while True:
                limpatela()
                try: #Tenta iniciar a conexão                         
                    conexao = Conexao.connect("DSN="+str(ConfiguracoesIni.dsn)) #Tenta iniciar conexão com o banco
                    print("Conexão [OK]")
                except Conexao.Error as ErroAoAbrirConexao: #Se não conseguir conexão, cai aqui e tenta de novo
                    QuantidadeErros = QuantidadeErros + 1
                    DescricaoErro = str(ErroAoAbrirConexao)
                    UltimaTentativa = dataAgora.strftime("%d/%m/%Y AS %H:%M:%S")
                    Escrevelog(2,DescricaoErro)
                    print("Erro de conexao por favor, contate o suporte")
                    time.sleep(5)
                    #Codigo abaixo foi comentado pois o mesmo server somente para exibir um contador
                    #Mas como não vai ter parte grafica por enquanto, não vai precisar
                    #segundosParaProximaSincronizacaoEmCasoDeFalha = int(ConfiguracoesIni.IntervaloDeTempoEntreTentativasEmCasoDeErro)
                    #while(segundosParaProximaSincronizacaoEmCasoDeFalha >= 0):
                    #    print("Falha na sincronização")
                    #    print("Uma nova tentativa sera realizada em: "+str(Data.timedelta(seconds=segundosParaProximaSincronizacaoEmCasoDeFalha)))
                    #    print("Tentativa "+str(QuantidadeErros)+" de "+str(ConfiguracoesIni.QuantidadeTentativasEmCasoDeErro))
                    #    segundosParaProximaSincronizacaoEmCasoDeFalha = segundosParaProximaSincronizacaoEmCasoDeFalha - 1
                    #time.sleep(int(ConfiguracoesIni.IntervaloDeTempoEntreTentativasEmCasoDeErro))
                    limpatela()
                    SincTotal()
                    break
                #Termina o bloco que tenta iniciar a conexão            
                try: #Inicia a tentativa de sincronizar tudo 
                    print("SINCRONIZANDO!")                      
                    cursorNuvem = conexao.cursor()                                        
                    cursorNuvem.execute("{CALL procSincronizacao()}")                    
                    UltimaSincronizaoComSucesso = dataAgora.strftime("%d/%m/%Y AS %H:%M:%S")
                    conexao.commit() 
                    conexao.close()  
                    print("Sincronizado com sucesso!")
                    time.sleep(10)    
                    #Codigo abaixo foi comentado pois o mesmo server somente para exibir um contador
                    #Mas como não vai ter parte grafica por enquanto, não vai precisar
                    #segundosParaProximaSincronizacao = int(ConfiguracoesIni.AtualizacaoAutomatica)
                    #while(segundosParaProximaSincronizacao >= 0):
                    #    print("Sincronizado com sucesso!")
                    #    print("Proxima atualizacão em: "+str(Data.timedelta(seconds=segundosParaProximaSincronizacao)))
                    #    segundosParaProximaSincronizacao = segundosParaProximaSincronizacao - 1
                    Escrevelog(1,"")
                    #time.sleep(int(ConfiguracoesIni.AtualizacaoAutomatica))
                    limpatela()
                    QuantidadeErros = 0
                    break
                except Conexao.Error as ErroAoSincronizar:                    
                    QuantidadeErros = QuantidadeErros + 1
                    DescricaoErro = str(ErroAoSincronizar)
                    UltimaTentativa = dataAgora.strftime("%d/%m/%Y AS %H:%M:%S") 
                    Escrevelog(2,DescricaoErro)
                    print("Erro ao Sincronizar! Por favor, contete o suporte")
                    time.sleep(10)    
                    #Codigo abaixo foi comentado pois o mesmo server somente para exibir um contador
                    #Mas como não vai ter parte grafica por enquanto, não vai precisar
                    #segundosParaProximaSincronizacaoEmCasoDeFalha = int(ConfiguracoesIni.IntervaloDeTempoEntreTentativasEmCasoDeErro)
                    #while(segundosParaProximaSincronizacaoEmCasoDeFalha >= 0):
                    #    print("Falha na sincronização")
                    #    print("Tentativa "+str(QuantidadeErros)+" de "+str(ConfiguracoesIni.QuantidadeTentativasEmCasoDeErro))
                    #    print("Uma nova tentativa sera realizada em: "+str(Data.timedelta(seconds=segundosParaProximaSincronizacaoEmCasoDeFalha)))
                    #    segundosParaProximaSincronizacaoEmCasoDeFalha = segundosParaProximaSincronizacaoEmCasoDeFalha - 1
                    #time.sleep(int(ConfiguracoesIni.IntervaloDeTempoEntreTentativasEmCasoDeErro))
                    limpatela()
                    SincTotal()
                    break                
                #Termina o bloco que tenta sincronizar tudo                
        except KeyboardInterrupt:
            Escrevelog(3,"") #Escreve no log que o usuario parou a aplicação
            sys.exit() #Fecha a aplicação caso o usuario aperte CTRL+C no console
        except SystemExit:
            Escrevelog(0,"")

#SincTotal() #Aqui inicia a aplicação

def VerificaUltimaSincronizacao():
    ultimaAtualizacao = os.stat('LogSincronizacao.txt').st_mtime
    ultimaAtualizacao = datetime.fromtimestamp(ultimaAtualizacao)
    horaAgora = Data.datetime.now()
    horaUltimaSincronizacao =  horaAgora - ultimaAtualizacao
    diferencaEmMinutos = horaUltimaSincronizacao.total_seconds() / 60
    if(diferencaEmMinutos <= 60 and diferencaEmMinutos >= 55):
        print('Aguarde, a proxima sincronização ocorrera nos proximos 5 minutos!')
        print('Pressione qualquer tecla para fechar')
        time.sleep(10)
    else:
        SincTotal()

VerificaUltimaSincronizacao()