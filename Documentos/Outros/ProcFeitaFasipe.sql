IF OBJECT_ID ('DBA.procSincronizacao') IS NOT NULL
	DROP PROCEDURE DBA.procSincronizacao
GO

create procedure DBA.procSincronizacao()
begin
  /*
  IDEIA:
  Tentar fazer um Where que só traga os registros dos ultimos 2 meses ou do ultimo mes
  no select das tabelas maiores, como por exemplo, a Orcamento balcao

  IDEIA 2:
  Trazer somente os orcamento balcoes nao sincronizador, para diminuir o tempo da proc

  IDEIA 3:
  SE O CPF DO USUARIO REGISTRADO JÁ ESTIVER REGISTRADO, SOMENTE ATUALIZA AS BASES

  DICA 1:
  ATUALIZAR PRIMEIRO OS CLIENTES ANTES DE ATUALIZAR ORCAMENTO E PARCELAS
  */
  declare vCodCliente integer;
  declare vCodOrcamento integer;
  declare vDebugCodigo integer;
  //Cria temporárias locais	
  select * into #Cliente from DBA.Cliente;
  select * into #CondicaoPagamento from DBA.CondicaoPagamento;
  select * into #Curso from DBA.Curso;
  select * into #FormaPagamento from DBA.FormaPagamento;
  select * into #GrupoEvento from DBA.GrupoEvento;
  select CodEmpresa,CodOrcamento,CodVendedor,CodCliente,DataDigitacao into #OrcamentoBalcao from DBA.OrcamentoBalcao;
  select CodEmpresa,CodOrcamento,Item,CodProduto,NomeProduto,ValorDescontoDigitadoProduto,ValorAcrescimoDigitadoProduto,ValorDescontoTotal1,ValorItem,CodCliente into #ItemOrcamentoBalcao from DBA.ItemOrcamentoBalcao;
  select * into #MovimentacaoEstoque from DBA.MovimentacaoEstoque;
  select * into #ParcelaOrcamentoBalcao from DBA.ParcelaOrcamentoBalcao;
  select * into #Produto from DBA.Produto;
  select * into #Projeto from DBA.Projeto;
  select * into #TabelaPrecoEvento from DBA.TabelaPrecoEvento;
  select * into #ProjetoXCurso from DBA.ProjetoXCurso;
  //Cria temporárias nuvem
  select codusuariocliente,codcliente,nome,tipopessoa,nomefantasia,tipocliente,datacadastro,cnpj_cpf,consulta_cnpj_cpf,email,risco,percentualcomissao,limitecredito,percentualdesconto,emitecartacobranca,aliquotairrf,celular,diavencimento,valormensalidade,ativo,dataprimeiracompra,valorprimeiracompra,valorultimacompra,dataultimacompra,saldoatual,maiorsaldo,datamaiorcompra,datamaiorsaldo,numerocompra,valorcompra,diasmediocompra,valormediocompra,maioratrazo,quantidadechequedevolvido,codvendedorultimacompra,usuarioultimacompra,horaultimacompra,numeroultimavenda,fator,valorcredito,consultausuario,codempresa,cadastroatualizado,emiteboleto,codcurso,Emitenotaservico,password,remember_token,consulta_cnpj_cpf_antigo,sincronizado into #nuvem_Cliente from nuvem_Cliente;
  select * into #nuvem_Condicao_Pagamento from nuvem_Condicao_Pagamento;
  select * into #nuvem_Curso from nuvem_Curso;
  select * into #nuvem_Forma_Pagamento from nuvem_Forma_Pagamento;
  select * into #nuvem_GrupoEvento from nuvem_GrupoEvento;
  select CodOrcamentoBalcao,id,CodEmpresa,CodOrcamento,Item,CodProduto,PrecoUnitario,PrecoTabela,NomeProduto,ValorItem,PercentualDesconto,PrecoUtilizado,CodTipoProduto,PercentualDescontoPadrao,ValorDescontoPadrao,status,CodTipoMovimentoEstoque,status_id,DataUltimaInteracaoPagSeguro,DataPagamentoPagSeguro,UltimaInteracaoPagSeguro,token,TransacaoCode,InscricaoFinalizada,sincronizado,data_sincronizacao into #nuvem_Item_orcamento_balcao from nuvem_Item_orcamento_balcao;
  select id,CodEmpresa,CodMovimentacaoEstoque,CodTipoMovimentoEstoque,CodProduto,NumeroDocumento,DataDocumento,DataMovimento,Origem,CodUnidadeMedida,Quantidade,ValorUnitario,CodOrigem,NomeUsuario,EstacaoTrabalho,DataInclusao,HoraInclusao,PrecoVenda,PrecoVenda2,PrecoVenda3,PrecoVenda4,PrecoVenda5,PrecoVendaAnterior,ItemOrigem,CodClienteFornecedor,SaldoAtual,SaldoInicial,DataSaldoInicial,DataSaldoAtual,QuantidadeConvertida,CodCliente,TipoNota,DataVencimentoPagamento,PagamentoRealizado,PagamentoRealizadoVencimento,CodUsuarioCliente,CodOrcamentoBalcao,PagamentoFinalizado,token,TransacaoCode,status_id,DataUltimaInteracaoPagSeguro,DataPagamentoPagSeguro,UltimaInteracaoPagSeguro,InscricaoFinalizada,sincronizado,data_sincronizacao into #nuvem_Movimentacao_estoque from nuvem_Movimentacao_estoque;
  select CodOrcamentoBalcao,CodEmpresa,CodOrcamento,CodVendedor,CodCliente,DataDigitacao,VendaEfetuada,CodCondicaoPagamento,ValorTotal,DataInclusao,HoraInclusao,FlagLivre1,CodTipoOperacaoSaida,NomeCliente,ValorTotalBruto,PesquisaPadraoIntegerLivre1,CodUsuarioCliente,forma_pagamento_id,DataInscricao,ProjetoId,DataPagamentoPagSeguro,DataUltimaInteracaoPagSeguro,UltimaInteracaoPagSeguro,sincronizado,data_sincronizacao,token,TransacaoCode,status_id,InscricaoFinalizada,grossAmount,intermediationRateAmount,intermediationFeeAmount,netAmount,PaymentLink,ObservacaoCancelamentoArteweb,EmailContaPagSeguro,TokenContaPagseguro into #nuvem_Orcamento_balcao from nuvem_Orcamento_balcao;
  select id,codempresa,numeroparcela,valorparcela,codorcamento,datavencimento,codformapagamento,valorparcelaoriginal,datavencimentooriginal,CodOrcamentoBalcao,status_id,DataUltimaInteracaoPagSeguro,DataPagamentoPagSeguro,UltimaInteracaoPagSeguro,token,TransacaoCode,InscricaoFinalizada,sincronizado,data_sincronizacao into #nuvem_Parcela_orcamento_balcao from dba.nuvem_Parcela_orcamento_balcao;
  select id,CodProduto,Nome,DataCadastro,CodGrupoProduto,CodCategoriaProduto,PrecoVenda,CodClienteUltimaVenda,DataUltimaVenda,HoraUltimaVenda,NumeroUltimaVenda,QuantidadeUltimaVenda,UltimoPrecoVenda,QuantidadeVendida,Recado,DataPrecoVenda,PrecoVendaAnterior,PrecoVenda2,PrecoVenda3,PrecoVenda4,PrecoVenda5,DataInicioEvento,DataFimEvento,QuantidadeHorasEvento,Ensalamento,CodCurso,HoraInicio,HoraFim,CodProduto_Int,EventoObrigatorio,sincronizado,data_sincronizacao into #nuvem_produto from nuvem_produto;
  select * into #nuvem_projeto from nuvem_projeto;
  select * into #nuvem_projeto_Valores from nuvem_projeto_Valores;
  select * into #nuvem_projeto_x_curso from nuvem_projeto_x_curso;
  set vDebugCodigo=25;
  --VERIFICAÇÕES LOCAIS
  --Verifica por novos eventos [OK]
  insert into dba.nuvem_projeto with auto name
    select CodEmpresa,CodProjeto,NomeProfessorResponsavel,PalavraChave,AreaConhecimentoCapes,DataInicio,DataTermino,PrevisaoHoras,Objetivos,Justificativa,ReferencialTeorico,Conteudo,Resultados,Cronograma,Recursos,Bibliografia,Aprovado,CodProduto,ObservacaoCPPE,Nome,TipoRepasseIES,TipoRepasseCoordenacao,TipoRepasseColegiado,ValorRepasseIES,ValorRepasseCoordenacao,ValorRepasseColegiado,Finalizado,DiasEvento,CodProfessor,Assinatura1,Funcao1,Assinatura2,Funcao2,TipoRepasseProfessor,ValorRepasseProfessor,CodCurso,Status,CodAssinaturaCertificado1,CodAssinaturaCertificado2,VariosCursos,CodDistribuicaoReceita,DataLiberacao,Observacao,DataInicioInscricoesOnLine,DataFimInscricoesOnLine,NomeBusca,ObservacaoSite,DataLimiteBoleto,QuantidadePalestrasObrigatorias,sincronizado,NOW(*) as Data_Sincronizacao from
      #Projeto where
      Sincronizado = 'n';
  --Verifica por novos cursos  [OK]  
  set vDebugCodigo=1;
  insert into DBA.nuvem_curso with auto name
    select CodCurso,Nome,CodCursoOrigem,CodInstituicaoEnsino,EstacaoTrabalhoInclusao,DataHoraInclusao,UsuarioInclusao,EstacaoTrabalhoAlteracao,DataHoraAlteracao,UsuarioAlteracao,HorasExtracurriculares,Colegiado,Coordenador,DisponivelAcademico,GrupoEvento from
      #Curso where
      Sincronizado = 'N';
  set vDebugCodigo=2;
  --Verifica por novos Projeto X Curso [OK]
  insert into dba.nuvem_projeto_x_curso with auto name
    select codprojeto,codcurso,FatorDivisaoHoras,now(*) as data_sincronizacao from
      #ProjetoXCurso where
      codprojeto = any(select CodProjeto from #projeto where sincronizado = 'n');
  set vDebugCodigo=3;
  --Verifica por novas tabelas de preço [OK]
  insert into dba.nuvem_projeto_Valores with auto name
    select codprojeto,precovenda,academico,DataInicial as Validade,now(*) as data_sincronizacao from
      #TabelaPrecoEvento where
      codprojeto = any(select codProjeto from #projeto where sincronizado = 'n');
  set vDebugCodigo=4;
  --Verifica por novos GrupoEvento [OK]
  insert into dba.nuvem_GrupoEvento with auto name
    select CodProjeto,CodGrupoEvento,CodCurso from
      #GrupoEvento where
      codprojeto = any(select codprojeto from #projeto where sincronizado = 'n');
  set vDebugCodigo=5;
  --Verifica por novos produtos [OK]
  insert into dba.nuvem_produto with auto name
    select codProduto,nome,PrecoVenda,MovimentaEstoque,CodCategoriaProduto,CodGrupoProduto,HoraInicio,HoraFim,Ensalamento,EventoObrigatorio,DataInicioEvento,DataFimEvento,QuantidadeHorasEvento,CodCurso,PercentualDescontoPadrao,Recado,sincronizado,now(*) as data_sincronizacao from
      #Produto where
      CodGrupoProduto = any(select codprojeto from #Projeto where sincronizado = 'n');
  set vDebugCodigo=6;
  --VERIFICAÇÕES NA NUVEM [OK]
  --Verifica por novos clientes registrados no site   [OK]
  select max(codCliente) into vcodCliente from DBA.Cliente; --Pega o ultimo codCliente para depois poder somar + 1 na hora do insert
  insert into DBA.Cliente with auto name
    select vCodCliente+number(*) as CodCliente,Nome,TipoPessoa,NomeFantasia,datacadastro,cnpj_cpf,email,celular,codempresa,codcurso,password,TipoCliente,Risco,ativo from
      #nuvem_cliente where
      sincronizado = 'n';
  set vDebugCodigo=7;
  --SEPARA SÓ OS CLIENTES QUE PRECISAM SER SINCRONIZADOS E OS COLOCA NUMA TABELA TEMPORARIA
  select CodCliente,CNPJ_CPF into #ClientesNaoSincronizados from DBA.nuvem_cliente where sincronizado = 'n';
  update #ClientesNaoSincronizados as CN join DBA.Cliente as C on(CN.CNPJ_CPF = C.CNPJ_CPF) set
    CN.CODCLIENTE = C.CODCLIENTE;
  select CodCliente,CNPJ_CPF into #ClienteParaSincronizar from #ClientesNaoSincronizados;
  --Atualiza a base da nuvem com o codCliente correto da baseLocal [ok]
  for CurCliente as C dynamic scroll cursor for
    select CodCliente as CurCodCliente,CNPJ_CPF as CurCNPJ_CPF from #ClienteParaSincronizar do
    update DBA.nuvem_cliente as N set N.CodCliente = CurCodCliente where N.CNPJ_CPF = CurCNPJ_CPF and N.Sincronizado = 'n' end for;
  set vDebugCodigo=8;
  --Atualiza o codCliente do orcamentobalcao da nuvem com o codCliente da base local [OK]
  -- ATUALMENTE TRABALHANDO NESSE TRECHO DE CÓDIGO  
  --Verifica por novos orcamentobalcao no site [OK]
  select max(codorcamento) into vCodOrcamento from #OrcamentoBalcao; --Pega o ultimo CodOrcamento para depois poder somar + 1 na hora do insert
  insert into DBA.OrcamentoBalcao with auto name
    select vCodOrcamento+number as CodOrcamento,CodVendedor,CodCliente,DataDigitacao,VendaEfetuada,CodCondicaoPagamento,ValorTotal,DataInclusao,HoraInclusao,FlagLivre1,CodTipoOperacaoSaida,NomeCliente,ValorTotalBruto,PesquisaPadraoIntegerLivre1,sincronizado,EmailContaPagSeguro,CodOrcamentoBalcao as InteiroLivre3 from
      #nuvem_Orcamento_balcao where
      Sincronizado = 'n';
  set vDebugCodigo=9;
  --Verifica por novos itemOrcamentoBalcao no site [OK]
  select CodOrcamentoBalcao,codorcamento,sincronizado into #NuvemItemBalcao from DBA.nuvem_item_orcamento_balcao where sincronizado = 'n';
  select CodOrcamentoBalcao,CodOrcamento,sincronizado into #NuvemOrcamentoBalcao from DBA.nuvem_orcamento_balcao where sincronizado = 'n';
  update #NuvemItemBalcao as I join #NuvemOrcamentoBalcao as O on(I.CodOrcamentoBalcao = O.CodOrcamentoBalcao) set
    I.CodOrcamento = O.CodOrcamento;
  for CurItemOrcamentoBalcao as O dynamic scroll cursor for
    select CodOrcamentoBalcao as CurCodOrcamentoBalcao,CodOrcamento as CurCodOrcamento from #NuvemItemBalcao do
    update dba.nuvem_item_orcamento_balcao as N set N.CodOrcamento = CurCodOrcamento where N.CodOrcamentoBalcao = CurCodOrcamentoBalcao and sincronizado = 'n' end for;
  set vDebugCodigo=99;
  insert into DBA.ItemOrcamentoBalcao with auto name
    select CodEmpresa,CodOrcamento,Item,CodProduto,PrecoUnitario,PrecoTabela,NomeProduto,ValorItem,PercentualDesconto,PrecoUtilizado,CodTipoProduto,PercentualDescontoPadrao,ValorDescontoPadrao,status,sincronizado from
      #nuvem_Item_orcamento_balcao where
      Sincronizado = 'n';
  set vDebugCodigo=10;
  --Atualiza os items da orcamento balcao para terem os ids certos dos orcamentos [OK]
  update DBA.ItemOrcamentoBalcao as I join #OrcamentoBalcao as O on(I.CodOrcamento = O.InteiroLivre3) set
    I.CodOrcamento = O.CodOrcamento;
  set vDebugCodigo=11;
  --Verifica por novos ParcelaOrcamentoBalcao no site
  --INSERT INTO DBA.ParcelaOrcamentoBalcao WITH auto name
  --SELECT CodEmpresa, NumeroParcela, ValorParcela, CodOrcamento, DataVencimento, CodFormaPagamento, TipoForma, CodTipoTitulo, ValorParcelaOriginal, DataVencimentoOriginal, CodFormaPagamentoOriginal, TipoFormaOriginal, CodTipoTituloOriginal, ValorJuroFinanciamento
  --Atualiza a base local indicando que os Projetos foram sincronizados
  update DBA.Projeto set
    Sincronizado = 'S' where
    Sincronizado = 'N';
  set vDebugCodigo=12;
  --Atualiza a base local indicando que os cursos foram sincronizados
  update DBA.Curso set
    Sincronizado = 'S' where
    Sincronizado = 'N';
  set vDebugCodigo=13;
  --Atualiza a base da nuvem indicando que os clientes foram sincronizados
  --BUG -> Ver por que a data_sincronização esta sendo preenchida com #####
  update DBA.nuvem_cliente set
    sincronizado = 'S' where
    sincronizado = 'N';
  set vDebugCodigo=14
exception
  when others then
    insert into DBA.Teste with auto name select vDebugCodigo as STATUS;
    select * from DBA.Teste
end
GO

