IF OBJECT_ID ('DBA.procSincronizacao1') IS NOT NULL
	DROP PROCEDURE DBA.procSincronizacao1
GO

create procedure DBA.procSincronizacao1()
begin
  --Variaveis para debug e benchmark da proc
  declare vDebugCodigo integer;
  declare vCodigoMovEstoque integer;
  declare vCodCliente integer;
  declare vCodOrcamento integer;
  declare vPrimeiroCodMovEstoqueInserido integer;
  --declare vCodMovEstoque integer;
  //Cria tempor�rias locais	
  select * into #Cliente from DBA.Cliente;
  select * into #CondicaoPagamento from DBA.CondicaoPagamento;
  select * into #Curso from DBA.Curso;
  --select * into #FormaPagamento from DBA.FormaPagamento;
  select * into #GrupoEvento from DBA.GrupoEvento;
  select CodEmpresa,CodOrcamento,CodVendedor,CodCliente,DataDigitacao into #OrcamentoBalcao from DBA.OrcamentoBalcao;
  select CodEmpresa,CodOrcamento,Item,CodProduto,NomeProduto,ValorDescontoDigitadoProduto,ValorAcrescimoDigitadoProduto,ValorDescontoTotal1,ValorItem,CodCliente into #ItemOrcamentoBalcao from DBA.ItemOrcamentoBalcao;
  select * into #MovimentacaoEstoque from DBA.MovimentacaoEstoque;
  select * into #Produto from DBA.Produto;
  select * into #Projeto from DBA.Projeto;
  select * into #TabelaPrecoEvento from DBA.TabelaPrecoEvento;
  select * into #ProjetoXCurso from DBA.ProjetoXCurso;
  select * into #NumeroParcelaXProjeto from DBA.NumeroParcelaXProjeto;
  //Cria tempor�rias nuvem
  select codusuariocliente,codcliente,nome,tipopessoa,nomefantasia,tipocliente,datacadastro,cnpj_cpf,consulta_cnpj_cpf,email,risco,percentualcomissao,limitecredito,percentualdesconto,emitecartacobranca,aliquotairrf,celular,diavencimento,valormensalidade,ativo,dataprimeiracompra,valorprimeiracompra,valorultimacompra,dataultimacompra,saldoatual,maiorsaldo,datamaiorcompra,datamaiorsaldo,numerocompra,valorcompra,diasmediocompra,valormediocompra,maioratrazo,quantidadechequedevolvido,codvendedorultimacompra,usuarioultimacompra,horaultimacompra,numeroultimavenda,fator,valorcredito,consultausuario,codempresa,cadastroatualizado,emiteboleto,codcurso,Emitenotaservico,password,remember_token,consulta_cnpj_cpf_antigo,sincronizado into #nuvem_Cliente from nuvem_Cliente where sincronizado = 'n';
  --select * into #nuvem_Condicao_Pagamento from nuvem_Condicao_Pagamento;
  select * into #nuvem_Curso from nuvem_Curso;
  --select * into #nuvem_Forma_Pagamento from nuvem_Forma_Pagamento;
  select * into #nuvem_GrupoEvento from nuvem_GrupoEvento;
  select CodOrcamentoBalcao,id,CodEmpresa,CodOrcamento,Item,CodProduto,PrecoUnitario,PrecoTabela,NomeProduto,ValorItem,PercentualDesconto,PrecoUtilizado,CodTipoProduto,PercentualDescontoPadrao,ValorDescontoPadrao,status,CodTipoMovimentoEstoque,status_id,DataUltimaInteracaoPagSeguro,DataPagamentoPagSeguro,UltimaInteracaoPagSeguro,token,TransacaoCode,InscricaoFinalizada,sincronizado,data_sincronizacao into #nuvem_Item_orcamento_balcao from nuvem_Item_orcamento_balcao where sincronizado = 'n';
  select id,CodEmpresa,CodMovimentacaoEstoque,CodTipoMovimentoEstoque,CodProduto,NumeroDocumento,DataDocumento,DataMovimento,Origem,CodUnidadeMedida,Quantidade,ValorUnitario,CodOrigem,NomeUsuario,EstacaoTrabalho,DataInclusao,HoraInclusao,PrecoVenda,PrecoVenda2,PrecoVenda3,PrecoVenda4,PrecoVenda5,PrecoVendaAnterior,ItemOrigem,CodClienteFornecedor,SaldoAtual,SaldoInicial,DataSaldoInicial,DataSaldoAtual,QuantidadeConvertida,CodCliente,TipoNota,DataVencimentoPagamento,PagamentoRealizado,PagamentoRealizadoVencimento,CodUsuarioCliente,CodOrcamentoBalcao,PagamentoFinalizado,token,TransacaoCode,status_id,DataUltimaInteracaoPagSeguro,DataPagamentoPagSeguro,UltimaInteracaoPagSeguro,InscricaoFinalizada,sincronizado,data_sincronizacao into #nuvem_Movimentacao_estoque from nuvem_Movimentacao_estoque where sincronizado = 'n';
  select CodOrcamentoBalcao,CodEmpresa,CodOrcamento,CodVendedor,CodCliente,DataDigitacao,VendaEfetuada,CodCondicaoPagamento,ValorTotal,DataInclusao,HoraInclusao,FlagLivre1,CodTipoOperacaoSaida,NomeCliente,ValorTotalBruto,PesquisaPadraoIntegerLivre1,CodUsuarioCliente,forma_pagamento_id,DataInscricao,ProjetoId,DataPagamentoPagSeguro,DataUltimaInteracaoPagSeguro,UltimaInteracaoPagSeguro,sincronizado,data_sincronizacao,token,TransacaoCode,status_id,InscricaoFinalizada,grossAmount,intermediationRateAmount,intermediationFeeAmount,netAmount,PaymentLink,ObservacaoCancelamentoArteweb,EmailContaPagSeguro,TokenContaPagseguro into #nuvem_Orcamento_balcao from nuvem_Orcamento_balcao where CodCondicaoPagamento is not null and CodCondicaoPagamento <> '' and sincronizado = 'n' and InscricaoFinalizada = 1;
  select id,codempresa,numeroparcela,valorparcela,codorcamento,datavencimento,codformapagamento,valorparcelaoriginal,datavencimentooriginal,CodOrcamentoBalcao,status_id,DataUltimaInteracaoPagSeguro,DataPagamentoPagSeguro,UltimaInteracaoPagSeguro,token,TransacaoCode,InscricaoFinalizada,sincronizado,data_sincronizacao into #nuvem_Parcela_orcamento_balcao from dba.nuvem_Parcela_orcamento_balcao where sincronizado = 'n';
  select id,CodProduto,Nome,DataCadastro,CodGrupoProduto,CodCategoriaProduto,PrecoVenda,CodClienteUltimaVenda,DataUltimaVenda,HoraUltimaVenda,NumeroUltimaVenda,QuantidadeUltimaVenda,UltimoPrecoVenda,QuantidadeVendida,Recado,DataPrecoVenda,PrecoVendaAnterior,PrecoVenda2,PrecoVenda3,PrecoVenda4,PrecoVenda5,DataInicioEvento,DataFimEvento,QuantidadeHorasEvento,Ensalamento,CodCurso,HoraInicio,HoraFim,CodProduto_Int,EventoObrigatorio,sincronizado,data_sincronizacao into #nuvem_produto from nuvem_produto where sincronizado = 'n';
  --Acredito que n�o vamos fazer o uso dessas tabelas, j� que n�o fazemos nenhum select delas, apenas inserts
  --Retirando elas, ganharemos varios segundos na sincroniza��o
  select * into #nuvem_projeto from nuvem_projeto;
  select * into #nuvem_projeto_Valores from nuvem_projeto_Valores;
  select * into #nuvem_projeto_x_curso from nuvem_projeto_x_curso;
  select * into #nuvem_numero_x_projeto from DBA.nuvem_numero_parcela_x_projeto;
  set vDebugCodigo=200;
  --VERIFICA��ES LOCAIS  
  --Verifica por novos eventos [OK]
  insert into dba.nuvem_projeto with auto name
    select CodEmpresa,CodProjeto,NomeProfessorResponsavel,PalavraChave,AreaConhecimentoCapes,DataInicio,DataTermino,PrevisaoHoras,Objetivos,Justificativa,ReferencialTeorico,Conteudo,Resultados,Cronograma,Recursos,Bibliografia,Aprovado,CodProduto,ObservacaoCPPE,Nome,TipoRepasseIES,TipoRepasseCoordenacao,TipoRepasseColegiado,ValorRepasseIES,ValorRepasseCoordenacao,ValorRepasseColegiado,Finalizado,DiasEvento,CodProfessor,Assinatura1,Funcao1,Assinatura2,Funcao2,TipoRepasseProfessor,ValorRepasseProfessor,CodCurso,Status,CodAssinaturaCertificado1,CodAssinaturaCertificado2,VariosCursos,CodDistribuicaoReceita,DataLiberacao,Observacao,DataInicioInscricoesOnLine,DataFimInscricoesOnLine,NomeBusca,ObservacaoSite,DataLimiteBoleto,QuantidadePalestrasObrigatorias,sincronizado,NOW(*) as Data_Sincronizacao from
      #Projeto where
      Sincronizado = 'n';
  --Verifica por novos cursos  [OK]  
  set vDebugCodigo=1;
  insert into DBA.nuvem_curso with auto name
    select CodCurso,Nome,CodCursoOrigem,CodInstituicaoEnsino,EstacaoTrabalhoInclusao,DataHoraInclusao,UsuarioInclusao,EstacaoTrabalhoAlteracao,DataHoraAlteracao,UsuarioAlteracao,HorasExtracurriculares,Colegiado,Coordenador,DisponivelAcademico,GrupoEvento from
      #Curso where
      Sincronizado = 'N';
  set vDebugCodigo=2;
  --Verifica por novos Projeto X Curso [OK]
  insert into dba.nuvem_projeto_x_curso with auto name
    select codprojeto,codcurso,FatorDivisaoHoras,now(*) as data_sincronizacao from
      #ProjetoXCurso where
      codprojeto = any(select CodProjeto from #projeto where sincronizado = 'n');
  set vDebugCodigo=3;
  --Verifica por novas tabelas de pre�o [OK]
  insert into dba.nuvem_projeto_Valores with auto name
    select codprojeto,precovenda,academico,DataInicial as Validade,now(*) as data_sincronizacao from
      #TabelaPrecoEvento where
      codprojeto = any(select codProjeto from #projeto where sincronizado = 'n');
  set vDebugCodigo=4;
  --Verifica por novos GrupoEvento [OK]
  insert into dba.nuvem_GrupoEvento with auto name
    select CodProjeto,CodGrupoEvento,CodCurso from
      #GrupoEvento where
      codprojeto = any(select codprojeto from #projeto where sincronizado = 'n');
  set vDebugCodigo=5;
  --Verifica por novos produtos [OK]
  insert into dba.nuvem_produto with auto name
    select codProduto,nome,PrecoVenda,MovimentaEstoque,CodCategoriaProduto,CodGrupoProduto,HoraInicio,HoraFim,Ensalamento,EventoObrigatorio,DataInicioEvento,DataFimEvento,QuantidadeHorasEvento,CodCurso,PercentualDescontoPadrao,Recado,sincronizado,now(*) as data_sincronizacao from
      #Produto where
      CodGrupoProduto = any(select codprojeto from #Projeto where sincronizado = 'n');
  set vDebugCodigo=6;
  insert into DBA.nuvem_numero_parcela_x_projeto with auto name
    select CodProjeto,NumeroParcela as numero_parcela,CodCurso from #NumeroParcelaXProjeto where CodProjeto = any(select CodProjeto from #Projeto where Sincronizado = 'n');
  --===================================================================================================================
  --O Trecho abaixo, chama a proc para sincronizar dados que j� est�o na nuvem mas foram alterados na base local
  --Por exemplo, alteraram o nome de um evento, o valor ou data
  --Resolvi criar uma segunda Proc para fazer isso para n�o ficar muito grande e confusa essa e tamb�m facilitar
  --futuras manuten��es 
  --===================================================================================================================
  call DBA.procSincronizacaoAtualizacao();
  --Verifica por novos clientes registrados no site   [OK]
  select max(codCliente) into vcodCliente from DBA.Cliente; --Pega o ultimo codCliente para depois poder somar + 1 na hora do insert
  insert into DBA.Cliente with auto name
    select vCodCliente+number(*) as CodCliente,Nome,TipoPessoa,NomeFantasia,datacadastro,cnpj_cpf,email,celular,codempresa,codcurso,password,TipoCliente,Risco,ativo from
      #nuvem_cliente where
      sincronizado = 'n';
  set vDebugCodigo=7;
  --SEPARA S� OS CLIENTES QUE PRECISAM SER SINCRONIZADOS E OS COLOCA NUMA TABELA TEMPORARIA [OK]
  select CodCliente,CNPJ_CPF into #ClientesNaoSincronizados from DBA.nuvem_cliente where sincronizado = 'n';
  update #ClientesNaoSincronizados as CN join DBA.Cliente as C on(CN.CNPJ_CPF = C.CNPJ_CPF) set
    CN.CODCLIENTE = C.CODCLIENTE;
  select CodCliente,CNPJ_CPF into #ClienteParaSincronizar from #ClientesNaoSincronizados;
  set vDebugCodigo=8;
  --Atualiza a base da nuvem com o codCliente correto da baseLocal [ok] 
  for CurCliente as C dynamic scroll cursor for
    select CodCliente as CurCodCliente,CNPJ_CPF as CurCNPJ_CPF from #ClienteParaSincronizar do
    update DBA.nuvem_cliente as N set N.CodCliente = CurCodCliente where N.CNPJ_CPF = CurCNPJ_CPF and N.Sincronizado = 'n' end for;
  set vDebugCodigo=9;
  --Atualiza o CodCliente do OrcamentoBalcao da nuvem com os codigos corretos [OK]
  select CodCliente,CodUsuarioCliente into #NuvemOrcamentoBalcaoPraAtualizarClientes from DBA.nuvem_orcamento_balcao where sincronizado = 'n' and CodCondicaoPagamento is not null and CodCondicaoPagamento <> '' and CodCliente is not null;
  select CodCliente,CodUsuarioCliente into #NuvemClientesJaAtualizados from DBA.nuvem_cliente where sincronizado = 'n';
  update #NuvemOrcamentoBalcaoPraAtualizarClientes as O join #NuvemClientesJaAtualizados as C on(O.CodUsuarioCliente = C.CodUsuarioCliente) set O.CodCliente = C.CodCliente where O.CodUsuarioCliente = C.CodUsuarioCliente;
  for CurOrcamentoBalcaoCliente as OBC dynamic scroll cursor for
    select CodCliente as CurCodCliente,CodUsuarioCliente as CurCodUsuarioCliente from #NuvemOrcamentoBalcaoPraAtualizarClientes do
    update DBA.nuvem_orcamento_balcao as N set N.CodCliente = CurCodCliente where N.CodUsuarioCliente = CurCodUsuarioCliente end for;
  set vDebugCodigo=91;
  --BAIXA PRA BASE LOCAL OS ORCAMENTOBALCAO DA NUVEM J� COM O CODCLIENTE [ok]
  select CodVendedor,CodCliente,DataDigitacao,VendaEfetuada,CodCondicaoPagamento,ValorTotal,DataInclusao,HoraInclusao,FlagLivre1,CodTipoOperacaoSaida,NomeCliente,ValorTotalBruto,PesquisaPadraoIntegerLivre1,EmailContaPagSeguro,CodOrcamentoBalcao,CodEmpresa into #NuvemOrcamentoAtualizada from DBA.nuvem_orcamento_balcao where sincronizado = 'n' and CodCondicaoPagamento is not null and CodCondicaoPagamento <> '' and CodCliente is not null;
  --select max(codorcamento) into vCodOrcamento from dba.OrcamentoBalcao; --Pega o ultimo CodOrcamento para depois poder somar + 1 na hora do insert
  insert into DBA.OrcamentoBalcao with auto name
    select(select max(CodOrcamento) from DBA.OrcamentoBalcao)+number(*) as CodOrcamento,CodVendedor,CodCliente,DataDigitacao,VendaEfetuada,CodCondicaoPagamento,ValorTotal,DataInclusao,HoraInclusao,FlagLivre1,CodTipoOperacaoSaida,NomeCliente,ValorTotalBruto,PesquisaPadraoIntegerLivre1,EmailContaPagSeguro,CodOrcamentoBalcao as InteiroLivre3,CodEmpresa from
      #NuvemOrcamentoAtualizada;
  set vDebugCodigo=10;
  --ATUALIZA O CODORCAMENTO DOS ORCAMENTO DA NUVEM [OK]
  select CodOrcamento,InteiroLivre3 into #OrcamentoBalcaoAtualizado from DBA.OrcamentoBalcao;
  select CodOrcamentoBalcao,CodOrcamento into #OrcamentoBalcaoNuvemParaAtualizar from DBA.nuvem_orcamento_balcao where sincronizado = 'n';
  update #OrcamentoBalcaoNuvemParaAtualizar as O join #OrcamentoBalcaoAtualizado as A on(O.CodOrcamentoBalcao = A.InteiroLivre3) set O.CodOrcamento = A.CodOrcamento where O.CodOrcamentoBalcao = A.InteiroLivre3;
  for CurOrcamentoBalcaoNuvem as OBN dynamic scroll cursor for
    select CodOrcamentoBalcao as CurCodOrcamentoBalcao,CodOrcamento as CurCodOrcamento from #OrcamentoBalcaoNuvemParaAtualizar do
    update dba.nuvem_orcamento_balcao as N set N.CodOrcamento = CurCodOrcamento where N.CodOrcamentoBalcao = CurCodOrcamentoBalcao and sincronizado = 'n' end for;
  set vDebugCodigo=11;
  --Atualiza as temporarias
  select CodOrcamentoBalcao,codorcamento,sincronizado into #NuvemItemBalcao from DBA.nuvem_item_orcamento_balcao where sincronizado = 'n';
  select CodOrcamentoBalcao,CodOrcamento,sincronizado into #NuvemOrcamentoBalcao from DBA.nuvem_orcamento_balcao where sincronizado = 'n' and CodCondicaoPagamento is not null and CodCondicaoPagamento <> '' and codorcamento is not null;
  --Atualiza o CodOrcamento do ItemOrcamentoBalcao das nuvens com o codigo ItemOrcamentoBalcao da base local [OK]
  update #NuvemItemBalcao as I join #NuvemOrcamentoBalcao as O on(I.CodOrcamentoBalcao = O.CodOrcamentoBalcao) set
    I.CodOrcamento = O.CodOrcamento where I.CodOrcamentoBalcao = O.CodOrcamentoBalcao;
  for CurItemOrcamentoBalcao as O dynamic scroll cursor for
    select CodOrcamentoBalcao as CurCodOrcamentoBalcao,CodOrcamento as CurCodOrcamento from #NuvemItemBalcao do
    update dba.nuvem_item_orcamento_balcao as N set N.CodOrcamento = CurCodOrcamento where N.CodOrcamentoBalcao = CurCodOrcamentoBalcao and sincronizado = 'n' end for;
  set vDebugCodigo=12;
  --Atualiza a tabela temporaria com os ItemsOrcamentoBalcao com ids atualizados antes de atualizar na base local [OK]
  select CodEmpresa,CodOrcamento,Item,CodProduto,QtdDevolvida,PrecoUnitario,ValorDesconto,PrecoTabela,NomeProduto,ValorItem,PercentualDesconto,PrecoUtilizado,CodTipoProduto,Quantidade,PercentualDescontoPadrao,ValorDescontoPadrao,status,sincronizado,garantia,CodUnidadeMedida,SituacaoTributaria into #NuvemItemOrcamentoBalcaoAtualizada from
    DBA.nuvem_item_orcamento_balcao where
    sincronizado = 'n' and CodOrcamento is not null and CodOrcamento <> '';
  --Verifica por novos itemOrcamentoBalcao no site [OK]
  insert into DBA.ItemOrcamentoBalcao with auto name
    select CodEmpresa,CodOrcamento,Item,Quantidade,CodProduto,PrecoUnitario,PrecoTabela,NomeProduto,ValorItem,PercentualDesconto,PrecoUtilizado,CodTipoProduto,PercentualDescontoPadrao,ValorDescontoPadrao,status,garantia,codunidademedida,SituacaoTributaria from
      #NuvemItemOrcamentoBalcaoAtualizada where
      Sincronizado = 'n';
  set vDebugCodigo=13;
  --Atualiza a movEstoque da nuvem [OK]
  update #nuvem_Movimentacao_estoque as n join #NuvemClientesJaAtualizados as c on(N.CodClienteFornecedor = C.CodUsuarioCliente) set
    n.CodCliente = C.CodCliente where
    N.CodClienteFornecedor = C.CodUsuarioCliente;
  set vDebugCodigo=14;
  --ATUALIZA A MOVIMENTA��O DE ESTOQUE DAS NUVENS COM O CODORIGEM e NUMERODOCUMENTO CERTO [OK]
  select CodOrcamentoBalcao,CodOrcamento into
    #OrcamentoBalcaoAtualizadoPraMovimentacao from
    DBA.nuvem_orcamento_balcao where sincronizado = 'n';
  select CodOrcamentoBalcao,NumeroDocumento,CodOrigem into
    #MovEstoque from DBA.nuvem_movimentacao_estoque where sincronizado = 'n';
  update #MovEstoque as M join
    #OrcamentoBalcaoAtualizadoPraMovimentacao as O on(M.CodOrcamentoBalcao = O.CodOrcamentoBalcao) set
    M.NumeroDocumento = O.CodOrcamento,
    M.CodOrigem = O.CodOrcamento where
    M.CodOrcamentoBalcao = O.CodOrcamentoBalcao;
  for CurMovimentacaoEstoque as CME dynamic scroll cursor for
    select CodOrcamentoBalcao as CurCodOrcamentoBalcao,CodOrcamento as CurCodOrcamento from #OrcamentoBalcaoAtualizadoPraMovimentacao do
    update DBA.nuvem_movimentacao_estoque set
      CodOrigem = CurCodOrcamento,NumeroDocumento = CurCodOrcamento where
      CodOrcamentoBalcao = CurCodOrcamentoBalcao and
      sincronizado = 'n' end for;
  set vDebugCodigo=15;
  --ATUALIZA A BASE LOCAL COM A MOVIMENTA��O ESTOQUE [OK]  
  select max(CodMovimentacaoEstoque) into vPrimeiroCodMovEstoqueInserido from dba.MovimentacaoEstoque;
  insert into DBA.MovimentacaoEstoque with auto name
    select(select max(CodMovimentacaoEstoque) from dba.MovimentacaoEstoque)+number(*) as CodMovimentacaoEstoque,CodEmpresa,2 as CodTipoMovimentoEstoque,CodProduto,NumeroDocumento,DataDocumento as DataInclusao,(case when HoraInclusao is null then '00:00' else HoraInclusao
      end) as HoraInclusao,DataMovimento,Origem,CodUnidadeMedida,Quantidade,CodSegundaUM,0 as QuantidadeSegundaUM,CodTerceiraUM,0 as QuantidadeTerceiraUM,1 as CodLocalArmazenagem,0 as ValorUnitario,CodOrigem,NumeroSerie,Observacao,Historico,ComplementoHistorico,NomeUsuario,'Sincronizador' as EstacaoTrabalho,0 as UltimoPrecoCompra,0 as PrecoVenda,0 as PrecoVendaAnterior,0 as ItemOrigem,(case when SaldoAtual is null then 0 else SaldoAtual end) as SaldoAtual,(case when SaldoInicial is null then 0 else SaldoInicial end) as SaldoInicial,DataSaldoInicial,DataSaldoAtual,FatorConversao,TipoFatorConversao,QuantidadeConvertida,0 as CodCor,0 as CodTamanho,CodCliente,CodFornecedor,TipoNota,'' as NumeroLote from DBA.nuvem_movimentacao_estoque where
      sincronizado = 'n';
  set vDebugCodigo=16;
  --Atualiza o CodMovimentacaoEstoque das Nuvens [OK]
  select CodMovimentacaoEstoque,NumeroDocumento into #MovimentoEstoqueAtualizado from DBA.MovimentacaoEstoque where CodMovimentacaoEstoque >= vPrimeiroCodMovEstoqueInserido;
  select CodMovimentacaoEstoque,NumeroDocumento into #MovimentoEstoquePraAtualizarNaNuvem from dba.nuvem_movimentacao_estoque where sincronizado = 'n';
  update #MovimentoEstoquePraAtualizarNaNuvem as M join #MovimentoEstoqueAtualizado as MA on(M.NumeroDocumento = MA.NumeroDocumento) set
    M.CodMovimentacaoEstoque = MA.CodMovimentacaoEstoque where M.NumeroDocumento = MA.NumeroDocumento;
  for CurMovimentacaoEstoque as AME dynamic scroll cursor for
    select CodMovimentacaoEstoque as CurCodMovimentacaoEstoque,NumeroDocumento as CurNumeroDocumento from #MovimentoEstoquePraAtualizarNaNuvem do
    update DBA.nuvem_movimentacao_estoque set
      CodMovimentacaoEstoque = CurCodMovimentacaoEstoque where
      NumeroDocumento = CurNumeroDocumento and
      sincronizado = 'n' end for;
  set vDebugCodigo=17;
  --Atualizar as parcelas    [OK]
  select CodEmpresa,CodOrcamento,CodOrcamentoBalcao,Sincronizado into #ParcelaOrcamentoBalcao from DBA.nuvem_parcela_orcamento_balcao where sincronizado = 'n';
  select CodOrcamento,CodOrcamentoBalcao into #OrcamentoBalcaoAtualizadoPraParcela from DBA.nuvem_orcamento_balcao where sincronizado = 'n';
  --Atualiza primeiro a nuvem [OK]
  update #ParcelaOrcamentoBalcao as P join #OrcamentoBalcaoAtualizadoPraParcela as O on(P.CodOrcamentoBalcao = O.CodOrcamentoBalcao) set
    P.CodOrcamento = O.CodOrcamento where P.CodOrcamentoBalcao = O.CodOrcamentoBalcao;
  for CurParcela as CP dynamic scroll cursor for
    select CodOrcamentoBalcao as CurCodOrcamentoBalcao,CodOrcamento as CurCodOrcamento from #ParcelaOrcamentoBalcao where sincronizado = 'n' do
    update DBA.nuvem_parcela_orcamento_balcao set CodOrcamento = CurCodOrcamento where CodOrcamentoBalcao = CurCodOrcamentoBalcao;
    set vDebugCodigo=18 end for;
  drop table #ParcelaOrcamentoBalcao; --Limpa a tabela Temporaria
  select CodEmpresa,NumeroParcela,ValorParcela,CodOrcamento,DataVencimento,CodFormaPagamento,CodAdmFinanceira,Numero,NumeroBanco,TipoForma,CodTipoTitulo,ValorParcelaOriginal,DataVencimentoOriginal,CodFormaPagamentoOriginal,TipoFormaOriginal,CodTipoTituloOriginal,CodOrcamentoBalcao,Sincronizado into #ParcelaOrcamentoBalcao from DBA.nuvem_parcela_orcamento_balcao where sincronizado = 'n';
  --Atualiza a base loca
  insert into DBA.ParcelaOrcamentoBalcao with auto name
    select CodEmpresa,NumeroParcela,ValorParcela,CodOrcamento,DataVencimento,CodFormaPagamento,CodAdmFinanceira,Numero,NumeroBanco,TipoForma,CodTipoTitulo,ValorParcelaOriginal,DataVencimentoOriginal,CodFormaPagamentoOriginal,TipoFormaOriginal,CodTipoTituloOriginal from #ParcelaOrcamentoBalcao;
  set vDebugCodigo=19;
  --Atualiza a base da nuvem indicando que tudo que nao estava sincronizado foi sincronizado
  update DBA.nuvem_cliente set
    sincronizado = 'S',
    Data_sincronizacao = now(*) where
    sincronizado = 'N';
  set vDebugCodigo=20;
  update DBA.nuvem_orcamento_balcao set
    Sincronizado = 's',
    data_sincronizacao = now(*) where
    Sincronizado = 'n';
  set vDebugCodigo=21;
  update DBA.nuvem_item_orcamento_balcao set
    Sincronizado = 's',
    Data_Sincronizacao = now(*) where
    Sincronizado = 'n';
  set vDebugCodigo=22;
  update DBA.nuvem_movimentacao_estoque set
    Sincronizado = 's',
    data_Sincronizacao = now(*) where
    Sincronizado = 'n';
  set vDebugCodigo=23;
  update DBA.nuvem_produto set
    Sincronizado = 's',
    Data_Sincronizacao = now(*) where
    Sincronizado = 'n';
  set vDebugCodigo=24;
  update DBA.nuvem_projeto set
    Sincronizado = 's',
    data_sincronizacao = now(*) where
    Sincronizado = 'n';
  set vDebugCodigo=25;
  update DBA.nuvem_projeto_valores set
    Sincronizado = 's',
    data_sincronizacao = now(*) where
    Sincronizado = 'n';
  set vDebugCodigo=26;
  update DBA.nuvem_projeto_x_curso set
    Sincronizado = 's',
    data_Sincronizacao = now(*) where
    Sincronizado = 'n';
  set vDebugCodigo=27;
  update DBA.nuvem_curso set
    Sincronizado = 's',
    data_sincronizacao = now(*) where
    Sincronizado = 'n';
  set vDebugCodigo=28;
  --Atualiza a base da nuvem indicando que tudo que nao estava sincronizado foi sincronizado
  update DBA.Projeto set
    Sincronizado = 'S',
    Data_Sincronizacao = now(*) where
    Sincronizado = 'N';
  set vDebugCodigo=29;
  --Atualiza a base local indicando que os cursos foram sincronizados
  update DBA.Curso set
    Sincronizado = 'S' where
    Sincronizado = 'N';
  set vDebugCodigo=30;
  --Atualiza a base da nuvem indicando que os clientes foram sincronizados     
  update DBA.Produto set
    Sincronizado = 's',
    data_Sincronizacao = now(*) where
    Sincronizado = 'n';
  set vDebugCodigo=31
exception
  when others then
    insert into DBA.LogDetalhadoSincronizador with auto name select vDebugCodigo as TrechoCodigo,ERRORMSG(*) as DescricaoErro
end
GO

