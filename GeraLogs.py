import datetime as Data


def Escrevelog(TipoLog,descricaoErr):
    dataAgora = Data.datetime.now() #Pega data do dia e hora
    if TipoLog == 0: #TipoLog 0 = O usuario fechou o sincronizador
        SalvaLog = open("LogSincronizacao.txt", "a")
        SalvaLog.write("["+dataAgora.strftime("%d/%m/%Y %H:%M:%S")+"] - Sincronizador foi fechado pelo usuario"
        +"=====================================================================\n")
        SalvaLog.close()

    if TipoLog == 1: #TipoLog 1 = SUCESSO, sem falhas na sincroniza��o
        SalvaLog = open("LogSincronizacao.txt", "a")
        SalvaLog.write("["+dataAgora.strftime("%d/%m/%Y %H:%M:%S")+"] - Sincronizacao Concluida com sucesso!\n"
        +"=====================================================================\n")
        SalvaLog.close()
    if TipoLog == 2: #TipoLog 2 = N�o foi sincronizado
        SalvaLog = open("LogSincronizacao.txt", "a")
        SalvaLog.write("["+dataAgora.strftime("%d/%m/%Y %H:%M:%S")+"] - Falha na sincronizacao. MOTIVO:\n"+descricaoErr+"\n=====================================================================\n")
        SalvaLog.close()

    if TipoLog == 3: #TipoLog 3 = Quando o usuario fecha o console apertando CTRL+C
        SalvaLog = open("LogSincronizacao.txt", "a")
        SalvaLog.write("["+dataAgora.strftime("%d/%m/%Y %H:%M:%S")+"] - Sincronizacao parada pelo usuario(Entrou com CTRL+C no Console!)\n"
        +"=====================================================================\n")
        SalvaLog.close()

    if TipoLog == 4: #TipoLog 3 = Quando o usuario fecha o console apertando CTRL+C
        SalvaLog = open("LogSincronizacao.txt", "a")
        SalvaLog.write("["+dataAgora.strftime("%d/%m/%Y %H:%M:%S")+"] - Arquivo de configuracao nao encontrado ou com problema! Impossivel continuar\nPor favor, verifique se o arquivo de configuracao 'SincConfig.ini' esta na mesma pasta do executavel\n"
        +"=====================================================================\n")
        SalvaLog.close()

    if TipoLog == 4: #TipoLog = 4 Logs relacionados ao envio de emails
        SalvaLog = open("LogSincronizacao.txt", "a")
        SalvaLog.write("["+dataAgora.strftime("%d/%m/%Y %H:%M:%S")+"] -Falha ao enviar email. MOTIVO:\n"+descricaoErr
        +"=====================================================================\n")
        SalvaLog.close()
