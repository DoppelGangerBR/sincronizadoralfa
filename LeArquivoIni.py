import configparser
from GeraLogs import Escrevelog
import time
import os

EmailSincronizador = ""
SenhaSincronizador = ""
Destinatarios = ""
ServidorSmtp = ""
PortaSmtp = ""
dsn = ""
AtualizacaoAutomatica = ""
QuantidadeTentativasEmCasoDeErro = ""
IntervaloDeTempoEntreTentativasEmCasoDeErro = ""
Cliente = ""
def LeConfiguracoes():
    try:
        global dsn
        global AtualizacaoAutomatica
        global QuantidadeTentativasEmCasoDeErro
        global IntervaloDeTempoEntreTentativasEmCasoDeErro
        global EmailSincronizador
        global SenhaSincronizador
        global Destinatarios
        global ServidorSmtp
        global PortaSmtp
        global Cliente
        leitorIni = configparser.ConfigParser()
        leitorIni.read('SincConfig.ini')
        dsn = leitorIni.get('SERVIDOR','DSN')
        AtualizacaoAutomatica = leitorIni.get('SINCRONIZADOR','TempoSincronizacaoAutomaticaEmSegundos')
        QuantidadeTentativasEmCasoDeErro = leitorIni.get('SINCRONIZADOR','QuantideTentativasEmCasoDeErro')
        IntervaloDeTempoEntreTentativasEmCasoDeErro = leitorIni.get('SINCRONIZADOR', 'IntervaloDeTempoEntreTentativasEmCasoDeErro')
        EmailSincronizador = leitorIni.get('EMAIL', 'EmailSincronizador')
        SenhaSincronizador = leitorIni.get('EMAIL', 'SenhaSincronizador')
        Destinatarios = leitorIni.get('EMAIL', 'Destinatarios')
        ServidorSmtp = leitorIni.get('EMAIL', 'ServidorSmtp')
        PortaSmtp = leitorIni.get('EMAIL', 'PortaSmtp')
        Cliente = leitorIni.get('EMAIL','Cliente')
    except configparser.Error as e:  
        Escrevelog(4,'')
        os._exit(0)