import smtplib
import socket
import LeArquivoIni as Configuracoes
import GeraLogs
from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart



def EnviaEmail(DescricaoErro):    
    try:
        Configuracoes.LeConfiguracoes()        
        #Percebi que dependendo da empresa que cede internet pra a empresa que vai usar o Sincronizador, a porta 25 que e a padrao
        #Para enviar emails é bloqueada, entao fiz esse tratamento, ele tenta enviar do jeito normal, caso nao consiga
        #Envia usando SSL
        try:            
            ServidorSmtp = smtplib.SMTP(str(Configuracoes.ServidorSmtp))           
        except:
            ServidorSmtp = smtplib.SMTP_SSL(str(Configuracoes.ServidorSmtp),int(Configuracoes.PortaSmtp))
        ServidorSmtp.login(Configuracoes.EmailSincronizador, Configuracoes.SenhaSincronizador)  
        Mensagem = MIMEMultipart('alternative')
        Mensagem['Subject'] = 'Sincronizador Alfameta'
        Mensagem['From'] = 'Sincronizador Alfameta'
        Mensagem['To'] = str(Configuracoes.Destinatarios)
        #O Texto da mensagem é formatado em HTML Basico
        TextoMensagem = """<html>
        <head></head>
        <body>
        <h2><b>Sincronizador Alfameta</h2></b>
        Foram detectadas <b>falhas</b> durante o processo de sincronização!<br>
        O Serviço de sincronização foi <font color="red" size="3"><b>PARADO<b></font> para evitar possiveis inconsistências<br>
        <b>Cliente.:</b>"""+Configuracoes.Cliente+"""<br>
        <b>Descrição da falha.:</b><br><i>"""+DescricaoErro+"""</i></body></html>"""
        TextoFormatado = MIMEText(TextoMensagem, 'html')
        Mensagem.attach(TextoFormatado)
        ServidorSmtp.sendmail(Configuracoes.EmailSincronizador, Configuracoes.Destinatarios.split(','), Mensagem.as_string())
        ServidorSmtp.close()        
    except smtplib.SMTPException as ErroSmtp:
        GeraLogs.Escrevelog(5, str(ErroSmtp))
    except TimeoutError as ErroDeResposta:
        GeraLogs.Escrevelog(5, str(ErroDeResposta))